import { expect } from "chai";
import * as url from "url";
import { WebdriverIOElement, WebdriverIOElements } from "@UITypes/webelements";

export default class BasePage {

  public async openGoogle() {
    await browser.url(browser.config.googleUrl);
  }

  public async openIngenico() {
    await browser.url(browser.config.ingenicoUrl);
  }

  public checkUrl() {
    const currentUrl = browser.getUrl();
    expect(currentUrl).to.contain(
      this["absolutUrl"],
      `url should contain ${this["absolutUrl"]} but got ${currentUrl}`
    );
  }

}

