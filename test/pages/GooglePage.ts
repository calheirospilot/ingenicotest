import BasePage from './BasePage';


/**
 * sub page containing specific selectors and methods for a specific page
 */
class GooglePage extends BasePage {


  get searchBox()   { 
      return $("[name=q]"); }


  async searchGoogle (searchItem) {
    console.log('>>>Search item: ' + searchItem)
    let ele : any = await this.searchBox
    await ele.setValue(searchItem)
    await browser.keys("Enter")
  }


}

export default new GooglePage();
