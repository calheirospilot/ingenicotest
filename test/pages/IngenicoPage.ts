import { ChainablePromiseElement } from 'webdriverio';
import pause from 'webdriverio/build/commands/browser/pause';
import waitUntil from 'webdriverio/build/commands/browser/waitUntil';
import click from 'webdriverio/build/commands/element/click';
import waitForClickable from 'webdriverio/build/commands/element/waitForClickable';
import { threadId } from 'worker_threads';
import BasePage from './BasePage';


/**
 * sub page containing specific selectors and methods for a specific page
 */
class IngenicoPage extends BasePage {

get contatoVendas()   { return $("//h1[contains(text(),'Contato vendas')]"); }        

get inputFirstName() { return $('#edit-first-name');}
  
get inputLastName() { return $('#edit-last-name'); } 
      
get inputEmail(){ return $('#edit-e-mail');} 

get inputJobTitle() { return $('#edit-job-title'); } 
  
get inputCompanyName() { return $('#edit-company-name'); } 

get inputIndustry() { return $('#edit-industry'); }

get inputTellUsMore() {  return $('#edit-tell-us-more'); } 

get btnSubmit() { return $('//input[@id="edit-submit"]'); } 

      /**
     * a method to encapsule automation code to interact with the page
     * e.g. send informations firstname, lastname, email, jobtitle, companyname, tellusmore
     */
    async fillForm (firstname: string, lastname: string, email: string, jobtitle: string, companyname: string, tellusmore: string): Promise<void>  {

      if (this.contatoVendas.isExisting()){
        
        await this.inputFirstName.setValue(firstname);
        await this.inputLastName.setValue(lastname);
        await this.inputEmail.setValue(email);
        await this.inputJobTitle.setValue(jobtitle);
        await this.inputCompanyName.setValue(companyname);

        await this.boxSet('#edit-country', 'Brazil')
        await this.boxSet('#edit-industry', 'Digital Goods and services')

        await this.inputTellUsMore.setValue(tellusmore);
        
        
    }
  }

     /** 
    * a method to validate success message after submitting the form
      * e.g. After submitting the form, the message should be displayed: Your form was successfully submitted.
      */
    async validateMsg(): Promise<void> {

      await (await $('//input[@id="edit-submit"]')).click();
      
      const elem = await $('div[class="webform-confirmation__message"]')
      await expect(elem).toHaveText('Seu formulário foi enviado com sucesso.')
    
    }
          
    /** a method to select an option in a select box */  
    public async boxSet (boxset: string, option: string) {
      
      const selectBox = await $(boxset);
      const value = await selectBox.getValue();
      console.log(value); 
  
      await selectBox.selectByAttribute('value', option);
    }
}

export default new IngenicoPage();



