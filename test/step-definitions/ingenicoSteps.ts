import { Given, When, Then } from '@wdio/cucumber-framework';
import IngenicoPage from '../pages/IngenicoPage';

Given(/^Ingenico page is opened$/, async ()=>{

    await IngenicoPage.openIngenico();

})

When(/^click on Search sales > Let's talk$/, async ()=> {
   
    const letsTalk = await $('=Vamos conversar')
    await letsTalk.click()

    const saleContact = await $('=Contato vendas')
    await saleContact.click()
    
})

Then(/^fill in contact information (.*) (.*) (.*) (.*) (.*) (.*)$/, async (firstname, lastname, email, jobtitle, companyname, tellusmore)=> {
     
    await IngenicoPage.fillForm(firstname, lastname, email, jobtitle, companyname, tellusmore);

})

Then(/^validate success message$/, async ()=> {
     
    await IngenicoPage.validateMsg();

})


