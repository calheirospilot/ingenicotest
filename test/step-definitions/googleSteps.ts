import { Given, When, Then } from '@wdio/cucumber-framework';
import * as chai from 'chai'
import GooglePage from '../pages/GooglePage';


Given(/^Google page is opened$/, async ()=>{
    await GooglePage.openGoogle();
    await (await GooglePage.searchBox).click
})

When(/^Search with (.*)$/, async (searchItem)=> {
    GooglePage.searchGoogle(searchItem);
})

Then(/^Click on first search result$/, async()=>{
    let firstResult : any = await $('<h3>')
    await firstResult.click()
})

Then(/^URL should match (.*)$/, async (expectedUrl)=>{
    console.log('>>> Expected Url: ' + expectedUrl)
    let url : any = await browser.getUrl();
    chai.expect(url).to.equal(expectedUrl)
})


