Feature: Google Search
   
    @google
    Scenario Outline: Search company name via google using examples
        Given Google page is opened
        When Search with <SearchItem>
        Then Click on first search result
        Then URL should match <ExpectedURL>

        Examples:
             | TestID          | SearchItem | ExpectedURL |
             | INGENICO_TC001  | Ingenico.com   | https://ingenico.com/en  |
        

    @google
    Scenario: Search company name via google
        Given Google page is opened
        When Search with Ingenico.com
        Then Click on first search result
        Then URL should match https://ingenico.com/en


 
        
             