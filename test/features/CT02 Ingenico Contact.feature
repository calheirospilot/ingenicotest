Feature: Ingenico Contact

    @ingenico
    Scenario Outline: Send a sales contact
        Given Ingenico page is opened
        When click on Search sales > Let's talk
        Then fill in contact information <firstname> <lastname> <email> <jobtitle> <companyname> <tellusmore>
        Then validate success message
    
    Examples:
           | firstname | lastname | email                  |  jobtitle | companyname | tellusmore     |
           | thiago    | lima     | thiagoteste@gmail.com  |  qa       | ingenico    |  test          |

 
        
             