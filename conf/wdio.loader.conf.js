const wdio = require("./wdio.conf");

const TEST_ENV = process.env.TEST_ENV ? process.env.TEST_ENV : "dev";
const envConfig = require(`./env/wdio.${TEST_ENV}.conf`).config;

//merge config base and env
const config = {
  ...wdio.config,
  ...envConfig
};

exports.config = config;
