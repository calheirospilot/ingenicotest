# Ingenicotest



## Testing planning
- Set up a test automation framework using the WebdriverIO tool which allows to:

- Write scenarios in Gherkin (Cucumber) for a keyword search on Google

- Implement scenario pickle steps

- Run a test on one or more browsers (chrome, firefox, etc.)

- Set up a build in the CI to launch the tests, I let you choose any tools (Gitlab, Jenkins, circle CI….)

## Quick start
Install dependencies required to run the tests:
npm install

# run tests
npm test
